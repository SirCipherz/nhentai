nhentai
=======

.. code-block::

           _   _            _        _
     _ __ | | | | ___ _ __ | |_ __ _(_)
    | '_ \| |_| |/ _ \ '_ \| __/ _` | |
    | | | |  _  |  __/ | | | || (_| | |
    |_| |_|_| |_|\___|_| |_|\__\__,_|_|


あなたも変態。 いいね?

|travis|
|pypi|
|license|


nHentai est un outil en CLI pour télécharger des doujinshi depuis <http://nhentai.net>

============
Installation
============
.. code-block::

    git clone https://framagit.org/SirCipherz/nhentai.git
    cd nhentai
    sudo python3 setup.py install

=====
Utilisation
=====
**IMPORTANT**: Pour bypasser la limite de téléchargement de nhentai, tu devrais utiliser l'option `--cookie` suivie de ton cookie du site pour le sauvegarder.

*Le dossier de base dans lequel sera télécharger les doujinshi est le dossier dans lequel tu lance la commande.*


Mettre en place ton cookie contre les capcha:

.. code-block:: bash

    nhentai --cookie "YOUR COOKIE FROM nhentai.net"

Télécharger un doujinshi:

.. code-block:: bash

    nhentai --id=123855,123866

Télécharger des doujinshi depuis un fichier (une id de doujinshi par ligne):

.. code-block:: bash

    nhentai --file=doujinshi.txt

Rechercher un doujinshi et télécharger la première page:

.. code-block:: bash

    nhentai --search="tomori" --page=1 --download

Télécharger un certain tag précis:

.. code-block:: bash

    nhentai --tag lolicon --download --page=2

Télécharger tes favorites avec un délais:

.. code-block:: bash

    nhentai --favorites --download --delay 1

Changer le nom du dossier de sortie:

.. code-block:: bash

    nhentai --id 261100 --format '[%i]%s'

Variables pour le nom de dossier:

- %i: Id du doujinshi
- %t: Nom du doujinshi
- %s: Nom du traducteur
- %a: Nom de l'auteur du doujinshi


Autre options:

.. code-block::

    Options:
      # Operation options
      -h, --help            affiche ce message d'aide et quite
      -D, --download        télécharge un doujinshi
      -S, --show            affiche les informations du doujinshi

      # Doujinshi options
      --id=ID               id du/des doujinshi ex : 1,2,3
      -s KEYWORD, --search=KEYWORD
                            cherche des doujinshi par tag
      --tag=TAG             télécharger des doujinshi par tag
      -F, --favorites       lister/télécharger vos favoris

      # Multi-page options
      --page=PAGE           nombre de page
      --max-page=MAX_PAGE   nombre de page à rechercher récursivement

      # Download options
      -o OUTPUT_DIR, --output=OUTPUT_DIR
                            dossier de téléchargement
      -t THREADS, --threads=THREADS
                            nombre de thread à utiliser
      -T TIMEOUT, --timeout=TIMEOUT
                            durée du timeout
      -d DELAY, --delay=DELAY
                            délais entre chaque téléchargement
      -p PROXY, --proxy=PROXY
                            utiliser un proxy, par example: http://127.0.0.1:1080
      -f FILE, --file=FILE  lit les id depuis un fichier
      --format=NAME_FORMAT  formate le dossier de sortie

      # options de génération
      --html                génère un .html des images du dossier actuel
      --no-html             ne génère aucun .html après le téléchargement
      -C, --cbz             génère un .cbz à partir des images
      --rm-origin-dir       supprime le dossiers après avoir généré le cbz

      # options de nHentai
      --cookie=COOKIE       ajouter son cookie nhentai pour bypasser le capcha


==============
nHentai Mirror
==============
Si tu veux utiliser un mirroir, utilise un proxy pour `nhentai.net` et `i.nhentai.net`
Par example :

.. code-block:: 

    i.h.loli.club -> i.nhentai.net
    h.loli.club -> nhentai.net

Créer une variable système `NHENTAI` vers votre mirroir.

.. code-block:: bash

    NHENTAI=http://h.loli.club nhentai --id 123456


.. image:: ./images/search.png?raw=true
    :alt: nhentai
    :align: center
.. image:: ./images/download.png?raw=true
    :alt: nhentai
    :align: center
.. image:: ./images/viewer.png?raw=true
    :alt: nhentai
    :align: center

============
あなたも変態
============
.. image:: ./images/image.jpg?raw=true
    :alt: nhentai
    :align: center



.. |travis| image:: https://travis-ci.org/RicterZ/nhentai.svg?branch=master
   :target: https://travis-ci.org/RicterZ/nhentai

.. |pypi| image:: https://img.shields.io/pypi/dm/nhentai.svg
   :target: https://pypi.org/project/nhentai/

.. |license| image:: https://img.shields.io/github/license/ricterz/nhentai.svg
   :target: https://github.com/RicterZ/nhentai/blob/master/LICENSE
